//
//  UIColor+PopEatColors.h
//  PopEat
//
//  Created by CHARALAMPOS SPYROPOULOS on 31/08/2018.
//  Copyright © 2018 home. All rights reserved.
//

@import Foundation;
#import <UIKit/UIKit.h>

@interface UIColor (PopEatColors)

+ (UIColor *)brownishGreyColor;
+ (UIColor *)lipstickColor;
+ (UIColor *)white70Color;

@end
