//
//  UIColor+PopEatColors.m
//  PopEat
//
//  Created by CHARALAMPOS SPYROPOULOS on 31/08/2018.
//  Copyright © 2018 home. All rights reserved.
//

#import "UIColor+PopEatColors.h"

@implementation UIColor (PopEatColors)

+ (UIColor *)brownishGreyColor
{
    return [UIColor colorWithRed:102.0f/255.0f green:102.0f blue:102.0f/255.0f alpha:1.0];
}

+ (UIColor *)lipstickColor
{
    return [UIColor colorWithRed:217.0f/255.0f green:27.0f blue:68.0f/255.0f alpha:1.0];
}

+ (UIColor *)white70Color
{
    return [UIColor colorWithRed:255.0f/255.0f green:255.0f blue:255.0f/255.0f alpha:0.7];
}

@end
