//
//  Venue.h
//  PopEat
//
//  Created by CHARALAMPOS SPYROPOULOS on 01/09/2018.
//  Copyright © 2018 home. All rights reserved.
//

#import <Foundation/Foundation.h>

@import MapKit;

@interface Venue : NSObject

@property(nonatomic,assign) NSInteger ID;
@property(nonatomic,strong) NSString *name;
@property(nonatomic,assign) CLLocationCoordinate2D location;
@property(nonatomic,strong) NSString *address;
@property(nonatomic,strong) NSArray *formattedAddress;
@property(nonatomic,strong) NSString *postalCode;
@property(nonatomic,strong) NSString *city;
@property(nonatomic,strong) NSString *state;
@property(nonatomic,strong) NSString *foursquareID;
@property(nonatomic,strong) NSString *url;
@property(nonatomic,strong) NSString *phone;
@property(nonatomic,strong) NSString *country;
@property(nonatomic,strong) NSString *countryCode;
@property(nonatomic,strong) NSString *foursquareImageURL;
@property(nonatomic,strong) NSString *formattedPhone;
@property(nonatomic,assign) CGFloat distance;
@property(nonatomic,strong) NSString *categoryName;
@property(nonatomic,strong) NSString *site;
@property(nonatomic,strong) NSArray *images;

@end
