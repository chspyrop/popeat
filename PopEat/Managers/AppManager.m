//
//  AppManager.m
//  PopEat
//
//  Created by CHARALAMPOS SPYROPOULOS on 31/08/2018.
//  Copyright © 2018 home. All rights reserved.
//

#import "AppManager.h"
#import <CoreLocation/CoreLocation.h>

@import GoogleMaps;

NSString * const kUserDidUpdateLocationNotification = @"kUserDidUpdateLocationNotification";

@interface AppManager () <CLLocationManagerDelegate>

@property(nonatomic,strong) CLLocationManager *locationManager;
@property(nonatomic,assign,readwrite) BOOL isLocationEnabled;
@property(nonatomic,strong,readwrite) CLLocation *currentLocation;

@end

@implementation AppManager

#pragma mark - Singleton

+ (AppManager *)sharedManager
{
    static AppManager *_sharedManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedManager = [[AppManager alloc] init];
    });
    return _sharedManager;
}

- (instancetype)init
{
    if (self = [super init]) {
        
        self.locationManager = [[CLLocationManager alloc] init];
        self.locationManager.delegate = self;
        self.locationManager.activityType = CLActivityTypeFitness;
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        self.locationManager.distanceFilter = 5.0f;
        self.locationManager.pausesLocationUpdatesAutomatically = YES;
        
    }
    return self;
}

#pragma mark - Public

- (void)startUpdatingLocation
{
    [self p_startUpdatingLocation];
}

- (void)stopUpdatingLocation
{
    [self.locationManager stopUpdatingLocation];
}

- (void)reverseGeocodeForCordinates:(CLLocationCoordinate2D)coordinate completion:(void(^)(NSString* address))completionHandler
{
    [[GMSGeocoder geocoder] reverseGeocodeCoordinate:coordinate completionHandler:^(GMSReverseGeocodeResponse* response, NSError* error) {
        if (error) {
            completionHandler(@"");
        } else {
            if ([response results].count > 0) {
                GMSAddress* addressObj = [[response results] firstObject];
                completionHandler((NSString*)[addressObj.lines objectAtIndex:0]);
            } else {
                completionHandler(@"");
            }
        }
    }];
}

#pragma mark - Private

- (void)p_startUpdatingLocation
{
    CLAuthorizationStatus authorizationStatus = [CLLocationManager authorizationStatus];
    if (authorizationStatus == kCLAuthorizationStatusAuthorizedAlways || authorizationStatus == kCLAuthorizationStatusAuthorizedWhenInUse) {
        [self.locationManager startUpdatingLocation];
    } else {
        [self.locationManager requestWhenInUseAuthorization];
    }
}

#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    switch (status) {
        case kCLAuthorizationStatusAuthorizedWhenInUse:
            self.isLocationEnabled = YES;
            [self p_startUpdatingLocation];
            break;
        default:
            self.isLocationEnabled = NO;
            break;
    }
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    if(error.code != kCLErrorLocationUnknown) {
        self.isLocationEnabled = NO;
    }
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    self.isLocationEnabled = YES;
    
    CLLocation *location = [locations lastObject];
    
    if (!location || !CLLocationCoordinate2DIsValid(location.coordinate)) {
        return;
    }
    
    self.currentLocation = location;
    [[NSNotificationCenter defaultCenter] postNotificationName:kUserDidUpdateLocationNotification object:self];
    [self.locationManager stopUpdatingLocation];
}

@end
