//
//  AppManager.h
//  PopEat
//
//  Created by CHARALAMPOS SPYROPOULOS on 31/08/2018.
//  Copyright © 2018 home. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

extern NSString * const kUserDidUpdateLocationNotification;

@interface AppManager : NSObject

@property(nonatomic,strong,readonly) CLLocation *currentLocation;

+ (AppManager *)sharedManager;

- (void)startUpdatingLocation;
- (void)stopUpdatingLocation;

- (void)reverseGeocodeForCordinates:(CLLocationCoordinate2D)coordinate completion:(void(^)(NSString* address))completionHandler;

@end
