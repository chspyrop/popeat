//
//  FoursquareClient.h
//  PopEat
//
//  Created by CHARALAMPOS SPYROPOULOS on 31/08/2018.
//  Copyright © 2018 home. All rights reserved.
//

@import CoreLocation;

@class Venue;

#import <Foundation/Foundation.h>
#import <AFNetworking/AFHTTPSessionManager.h>

typedef void (^FoursquareClientCompletionBlock)(NSArray *responseObject, NSError *error);
typedef void (^FoursquareClientVenueDetailsCompletionBlock)(Venue *venueWitDetails, NSError *error);

@interface FoursquareClient : AFHTTPSessionManager

+ (instancetype)sharedClient;

- (NSURLSessionDataTask *)getFoursquareVenuesAround:(CLLocationCoordinate2D)coord completion:(FoursquareClientCompletionBlock)completion;
- (NSURLSessionDataTask *)getDetailsForVenue:(NSString *)foursquareID completion:(FoursquareClientVenueDetailsCompletionBlock)completion;
- (NSURLSessionDataTask *)getFoursquareImagesForVenue:(NSString *)foursquareID completion:(FoursquareClientCompletionBlock)completion;

@end
