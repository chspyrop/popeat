//
//  FoursquareClient.m
//  PopEat
//
//  Created by CHARALAMPOS SPYROPOULOS on 31/08/2018.
//  Copyright © 2018 home. All rights reserved.
//

#import "FoursquareClient.h"
#import "Venue.h"

static NSString * kCLIENTID = @"MCU0IVU4Q3ZF4DWTIGU4NT1034WXWXRUL1ALNRDRW51ZYC5O";
static NSString * kCLIENTSECRET = @"5FSE4R0BOCWF33NIU1QDHEWRUQARDIO2VBBVAB1Q4TDQ1VH3";

@implementation FoursquareClient

#pragma mark - Singleton

+ (instancetype)sharedClient
{
    static FoursquareClient *_sharedClient = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedClient = [[self alloc] initWithBaseURL:[NSURL URLWithString:@"https://api.foursquare.com/v2/"] sessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    });
    return _sharedClient;
}

- (instancetype)initWithBaseURL:(NSURL *)url sessionConfiguration:(NSURLSessionConfiguration *)configuration
{
    if (self = [super initWithBaseURL:url sessionConfiguration:configuration]) {
        
        self.securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
        
        AFJSONRequestSerializer *requestSerializer = [AFJSONRequestSerializer serializer];
        [requestSerializer setValue:@"gzip,deflate" forHTTPHeaderField:@"Accept-Encoding"];
        self.requestSerializer = requestSerializer;
        
        AFJSONResponseSerializer *responseSerializer = [AFJSONResponseSerializer serializer];
        responseSerializer.acceptableContentTypes = [NSSet setWithArray:@[@"application/json", @"text/plain", @"text/html", @"application/zip"]];
        self.responseSerializer = responseSerializer;
    }
    return self;
}

#pragma mark - Public

- (NSURLSessionDataTask *)getFoursquareVenuesAround:(CLLocationCoordinate2D)coord completion:(FoursquareClientCompletionBlock)completion
{
    NSString *tmpVenueCategoriesIDs;
    
//    tmpVenueCategoriesIDs = @"4d4b7104d754a06370d81259,4d4b7105d754a06374d81259"; //Arts & Entertainment + Food
    
    NSDictionary *params;
    if (!tmpVenueCategoriesIDs) {
        params = @{@"client_id": kCLIENTID, @"client_secret": kCLIENTSECRET, @"ll": [NSString stringWithFormat:@"%f,%f", coord.latitude, coord.longitude],@"v":@"20160420",@"intent": @"checkin",@"radius": @"20",@"limit":@"20",@"locale":@"en"};
    } else {
        params = @{@"client_id": kCLIENTID, @"client_secret": kCLIENTSECRET, @"ll": [NSString stringWithFormat:@"%f,%f", coord.latitude, coord.longitude],@"v":@"20160420",@"intent": @"checkin",@"radius": @"20",@"limit":@"20",@"locale":@"en",@"categoryId": tmpVenueCategoriesIDs};
    }
    
    return [self GET:@"venues/search" parameters:params progress:^(NSProgress * _Nonnull downloadProgress) {
        
        NSLog(@"Progress....");
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSDictionary *foursquareVenuesDictionary = [[responseObject objectForKey:@"response"] objectForKey:@"venues"];
        NSMutableArray *foursquareVenuesMutableArray = [[NSMutableArray alloc] initWithCapacity:0];
        
        for (NSDictionary *tmpFousquareVenue in foursquareVenuesDictionary)
        {
            NSLog(@"Venue:%@",tmpFousquareVenue);
            Venue *tmpVenue = [[Venue alloc] init];
            //
            tmpVenue.name = [tmpFousquareVenue valueForKey:@"name"];
            tmpVenue.location = CLLocationCoordinate2DMake([[[tmpFousquareVenue valueForKey:@"location"] valueForKey:@"lat"] doubleValue], [[[tmpFousquareVenue valueForKey:@"location"] valueForKey:@"lng"] doubleValue]);
            tmpVenue.address = [[tmpFousquareVenue valueForKey:@"location"] valueForKey:@"address"];
            tmpVenue.formattedAddress = [[tmpFousquareVenue valueForKey:@"location"] valueForKey:@"formattedAddress"];
            tmpVenue.postalCode = [[tmpFousquareVenue valueForKey:@"location"] valueForKey:@"postalCode"];
            tmpVenue.city = [[tmpFousquareVenue valueForKey:@"location"] valueForKey:@"city"];
            tmpVenue.state = [[tmpFousquareVenue valueForKey:@"location"] valueForKey:@"state"];
            tmpVenue.foursquareID = [tmpFousquareVenue valueForKey:@"id"];
            tmpVenue.phone = [tmpFousquareVenue valueForKey:@"phone"];
            
            tmpVenue.country = [[tmpFousquareVenue valueForKey:@"location"] valueForKey:@"country"];
            tmpVenue.countryCode = [[tmpFousquareVenue valueForKey:@"location"] valueForKey:@"cc"];
            tmpVenue.formattedPhone = [tmpFousquareVenue valueForKey:@"formattedPhone"];
            tmpVenue.distance = [[[tmpFousquareVenue valueForKey:@"location"] valueForKey:@"distance"] floatValue];
            
            tmpVenue.categoryName = [[[tmpFousquareVenue valueForKey:@"categories"] valueForKey:@"name"] firstObject];
            tmpVenue.site = @"";
            [foursquareVenuesMutableArray addObject:tmpVenue];
        }
        
        if (foursquareVenuesMutableArray.count > 0) {
            completion(foursquareVenuesMutableArray,nil);
        } else {
            completion(nil,nil);
        }

    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        completion(nil,error);

    }];
    
}

- (NSURLSessionDataTask *)getDetailsForVenue:(NSString *)foursquareID completion:(FoursquareClientVenueDetailsCompletionBlock)completion
{
    NSDictionary *params;
    params = @{@"client_id": kCLIENTID, @"client_secret": kCLIENTSECRET, foursquareID : @"VENUE_ID", @"v":@"20160420"};

    return [self GET:[NSString stringWithFormat:@"venues/%@",foursquareID] parameters:params progress:^(NSProgress * _Nonnull downloadProgress) {
        NSLog(@"Progress....");
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *foursquareVenueWithDetailsDictionary = [responseObject objectForKey:@"response"];
        Venue *tmpVenue = [[Venue alloc] init];
        tmpVenue.phone = [[[foursquareVenueWithDetailsDictionary valueForKey:@"venue"] valueForKey:@"contact"] valueForKey:@"formattedPhone"];
        tmpVenue.site = [[foursquareVenueWithDetailsDictionary valueForKey:@"venue"] valueForKey:@"url"];
        completion(tmpVenue,nil);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        completion(nil,error);
    }];
}

- (NSURLSessionDataTask *)getFoursquareImagesForVenue:(NSString *)foursquareID completion:(FoursquareClientCompletionBlock)completion
{
    NSDictionary *params;
    params = @{@"client_id": kCLIENTID, @"client_secret": kCLIENTSECRET, @"v":@"20160420", @"limit":@"10", @"offset":@"100", foursquareID : @"VENUE_ID"};
    
    return [self GET:[NSString stringWithFormat:@"venues/%@/photos",foursquareID] parameters:params progress:^(NSProgress * _Nonnull downloadProgress) {
        NSLog(@"Progress....");
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSArray *foursquareImagesVenueArray = [[[responseObject objectForKey:@"response"] objectForKey:@"photos"] objectForKey:@"items"];
        NSMutableArray *foursquareImagesVenueMutableArray = [[NSMutableArray alloc] initWithCapacity:0];
        
        for (NSDictionary *tmpFousquareImageForVenue in foursquareImagesVenueArray)
        {
            NSString *imageForVenue = [NSString stringWithFormat:@"%@%@%@",[tmpFousquareImageForVenue valueForKey:@"prefix"],@"300x300",[tmpFousquareImageForVenue valueForKey:@"suffix"]];
            [foursquareImagesVenueMutableArray addObject:imageForVenue];
        }
        completion(foursquareImagesVenueMutableArray,nil);

    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        completion(nil,error);
    }];
    
}


@end
