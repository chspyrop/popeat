//
//  main.m
//  PopEat
//
//  Created by CHARALAMPOS SPYROPOULOS on 30/08/2018.
//  Copyright © 2018 home. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
