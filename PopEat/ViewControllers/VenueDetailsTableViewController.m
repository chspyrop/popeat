//
//  VenueDetailsTableViewController.m
//  PopEat
//
//  Created by CHARALAMPOS SPYROPOULOS on 01/09/2018.
//  Copyright © 2018 home. All rights reserved.
//

#import "VenueDetailsTableViewController.h"
#import "FoursquareClient.h"
#import "VenueDetailsPhotosCollectionViewCell.h"
#import "UIImageView+AFNetworking.h"

@interface VenueDetailsTableViewController () <UICollectionViewDataSource, UICollectionViewDelegate>

@property (weak, nonatomic) IBOutlet UILabel *venueNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *venueTypeLabel;
@property (weak, nonatomic) IBOutlet UILabel *venueAddressLabel;
@property (weak, nonatomic) IBOutlet UILabel *venueURLLabel;
@property (weak, nonatomic) IBOutlet UILabel *venuePhoneLabel;
@property (weak, nonatomic) IBOutlet UICollectionView *venuePhotosCollectionView;

@end

@implementation VenueDetailsTableViewController

#pragma mark - View Controller

- (void)viewDidLoad {
    [super viewDidLoad];
    [self p_setup];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [[FoursquareClient sharedClient] getDetailsForVenue:self.venue.foursquareID completion:^(Venue *venueWitDetails, NSError *error) {
        if (venueWitDetails) {
            self.venue.site = venueWitDetails.site;
            self.venue.phone = venueWitDetails.phone;
        }
        if ([[error.userInfo valueForKey:NSLocalizedDescriptionKey] isEqualToString:@"Request failed: client error (429)"] ) { //Avoid quota_exceeded error from Foursquare API https://developer.foursquare.com/docs/announcements
            self.venue.site = @"quota_exceeded error from Foursquare API";
            self.venue.phone = @"quota_exceeded error from Foursquare API";
        }
        
        [self p_setupVenueDetails];

        [[FoursquareClient sharedClient] getFoursquareImagesForVenue:self.venue.foursquareID completion:^(NSArray *responseObject, NSError *error) {
            if (responseObject.count > 0) {
                self.venue.images = responseObject;
            }
            if ([[error.userInfo valueForKey:NSLocalizedDescriptionKey] isEqualToString:@"Request failed: client error (429)"] ) { //Avoid quota_exceeded error from Foursquare API https://developer.foursquare.com/docs/announcements
                NSMutableArray *foursquareImagesVenueMutableArray = [[NSMutableArray alloc] initWithCapacity:0];
                [foursquareImagesVenueMutableArray addObject:@"https://igx.4sqi.net/img/general/width960/43170088_9LmHeVQJ_W8UrYqnbAgy8_gdwcFwS1ouF3SFqKoPYMI.jpg"];
                [foursquareImagesVenueMutableArray addObject:@"https://igx.4sqi.net/img/general/width960/31562801_v5Qn75dITMnIsbYskY6pP9odql2-NHB4_XU7Azl6Bc0.jpg"];
                [foursquareImagesVenueMutableArray addObject:@"https://igx.4sqi.net/img/general/width960/23554104_xf-1nWKqAVtRHA8L9aZQBCpefT6FIfVpxXWLrkt7sEU.jpg"];
                [foursquareImagesVenueMutableArray addObject:@"https://igx.4sqi.net/img/general/width960/12141367_aPUsQv5oG012fKthhz1ZAaUjrYk02CXz3UX6EOQSXo8.jpg"];
                self.venue.images = foursquareImagesVenueMutableArray;
            }
            [self p_setupVenueImages];
        }];
    }];
}

#pragma mark - Private

- (void)p_setup
{
    self.venueNameLabel.text = self.venueTypeLabel.text = self.venueAddressLabel.text = self.venueURLLabel.text = self.venuePhoneLabel.text = @"";
    if (self.venue) {
        
        self.navigationItem.title = @"Selected place";
        
        self.venueNameLabel.text = self.venue.name;
        self.venueTypeLabel.text = self.venue.categoryName;
        self.venueAddressLabel.text = [self.venue.formattedAddress componentsJoinedByString: @","];
    }
}

- (void)p_setupVenueDetails
{
    self.venueURLLabel.text = self.venue.site;
    self.venuePhoneLabel.text = self.venue.phone;
}

- (void)p_setupVenueImages
{
    [self.venuePhotosCollectionView reloadData];
}

#pragma mark - UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.venue.images.count;
}


- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(159, 159);
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    VenueDetailsPhotosCollectionViewCell *cell = (VenueDetailsPhotosCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"venuePhotosCollectionViewCell" forIndexPath:indexPath];
    [cell.photo setImageWithURL:[NSURL URLWithString:[self.venue.images objectAtIndex:indexPath.row]] placeholderImage:nil];
    return cell;
}


@end
