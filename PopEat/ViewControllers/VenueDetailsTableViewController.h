//
//  VenueDetailsTableViewController.h
//  PopEat
//
//  Created by CHARALAMPOS SPYROPOULOS on 01/09/2018.
//  Copyright © 2018 home. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Venue.h"

@interface VenueDetailsTableViewController : UITableViewController

@property(nonatomic,strong) Venue *venue;

@end
