//
//  MapViewController.m
//  PopEat
//
//  Created by CHARALAMPOS SPYROPOULOS on 30/08/2018.
//  Copyright © 2018 home. All rights reserved.
//

#import "MapViewController.h"
#import "AppManager.h"
#import "UIColor+PopEatColors.h"
#import "FoursquareClient.h"
#import "Venue.h"
#import "VenueDetailsTableViewController.h"

@import GoogleMaps;

@interface MapViewController () <GMSMapViewDelegate>

@property (nonatomic,strong,readwrite) CLLocation *currentLocation;

@property (weak, nonatomic) IBOutlet GMSMapView *mapView;
@property (weak, nonatomic) IBOutlet UIView *addressView;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;

@property (nonatomic,strong) NSArray *latestFoursquareVenues;
@property (nonatomic,strong) Venue *selectedVenue;

@end

@implementation MapViewController

#pragma mark - ViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self p_setup];
    
}

- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kUserDidUpdateLocationNotification object:nil];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"venueDetailsSegue"]) {
        VenueDetailsTableViewController *controller = (VenueDetailsTableViewController *)segue.destinationViewController;
        controller.venue = self.selectedVenue;
        return;
    }
}

#pragma mark - Private

- (void)p_setup {
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(p_handleUserDidUpdateLocation:) name:kUserDidUpdateLocationNotification object:nil];
    
    self.mapView.delegate = self;
    
    self.navigationItem.titleView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"logo.png"]];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    self.addressView.backgroundColor = [UIColor white70Color];
    self.addressView.layer.cornerRadius = 28;
    self.addressView.layer.masksToBounds = true;
    
    self.latestFoursquareVenues = [[NSArray alloc] init];
    self.selectedVenue = nil;
}

- (void)p_handleUserDidUpdateLocation:(NSNotification *)notification
{
    self.currentLocation = [[AppManager sharedManager] currentLocation];
    CLLocationCoordinate2D target = CLLocationCoordinate2DMake(self.currentLocation.coordinate.latitude, self.currentLocation.coordinate.longitude);
    self.mapView.camera = [GMSCameraPosition cameraWithTarget:target zoom:17];
    [self p_fetchForsquareVenuesArroundLocation:target];
}

- (void)p_fetchForsquareVenuesArroundLocation:(CLLocationCoordinate2D)target
{
    [[FoursquareClient sharedClient] getFoursquareVenuesAround:target completion:^(NSArray *responseObject, NSError *error) {
        NSArray *foursquareVenuesArray = responseObject;
        [self p_drawMarkersOnMapWith:foursquareVenuesArray];
    }];
}

- (void)p_drawMarkersOnMapWith:(NSArray *)Foursquarevenues {
    
    [self.mapView clear];
    self.latestFoursquareVenues = Foursquarevenues;
    
    for (Venue *tmpFousquareVenue in Foursquarevenues)
    {
        GMSMarker *marker = [GMSMarker markerWithPosition:tmpFousquareVenue.location];
        marker.title = tmpFousquareVenue.foursquareID;
        marker.icon = [UIImage imageNamed:@"pin"];
        marker.appearAnimation = kGMSMarkerAnimationPop;
        marker.map = self.mapView;
    }
}

#pragma mark - GMSMapViewDelegate

- (void)mapView:(GMSMapView *)mapView idleAtCameraPosition:(GMSCameraPosition *)position
{
    CLLocationCoordinate2D target = CLLocationCoordinate2DMake(position.target.latitude,position.target.longitude);

    [self p_fetchForsquareVenuesArroundLocation:target];

    [[AppManager sharedManager] reverseGeocodeForCordinates:target completion:^(NSString *address) {
        if(address) {
            self.addressLabel.text = [[address componentsSeparatedByString:@","] objectAtIndex:0];
        } else {
            self.addressLabel.text = @"";
        }
    }];

}

- (BOOL)mapView:(GMSMapView *)mapView didTapMarker:(GMSMarker *)marker {
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"foursquareID == %@", marker.title];
    NSArray *results = [self.latestFoursquareVenues filteredArrayUsingPredicate:predicate];
    
    if (results) {
        self.selectedVenue = (Venue *)[results firstObject];
        [self performSegueWithIdentifier:@"venueDetailsSegue" sender:self];
        return YES;
    } else {
        self.selectedVenue = nil;
        return YES;
    }
    
}

@end
