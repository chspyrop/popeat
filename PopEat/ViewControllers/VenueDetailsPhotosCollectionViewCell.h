//
//  VenueDetailsPhotosCollectionViewCell.h
//  PopEat
//
//  Created by CHARALAMPOS SPYROPOULOS on 02/09/2018.
//  Copyright © 2018 home. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VenueDetailsPhotosCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *photo;

@end
