//
//  VenueDetailsPhotosCollectionViewCell.m
//  PopEat
//
//  Created by CHARALAMPOS SPYROPOULOS on 02/09/2018.
//  Copyright © 2018 home. All rights reserved.
//

#import "VenueDetailsPhotosCollectionViewCell.h"

@implementation VenueDetailsPhotosCollectionViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self layoutIfNeeded];
    
    self.photo.layer.cornerRadius = 10.0f;
    self.photo.layer.masksToBounds = YES;
    
}

@end
