//
//  AppDelegate.h
//  PopEat
//
//  Created by CHARALAMPOS SPYROPOULOS on 30/08/2018.
//  Copyright © 2018 home. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

