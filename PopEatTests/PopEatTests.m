//
//  PopEatTests.m
//  PopEatTests
//
//  Created by CHARALAMPOS SPYROPOULOS on 30/08/2018.
//  Copyright © 2018 home. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "AppManager.h"

@interface PopEatTests : XCTestCase

@end

@implementation PopEatTests

- (void)setUp {
    [super setUp];
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testAppManagerreverseGeocode {
    
    [[AppManager sharedManager] stopUpdatingLocation];
    NSString *expectedAddress = @"Stratigopoulou 2";
    __block NSString *resultAddress;
    
    CLLocationCoordinate2D target = CLLocationCoordinate2DMake(37.987676395141172,23.743147552013397);
    
    XCTestExpectation *expectation = [self expectationWithDescription:@"reverseGeocodeForCordinates.."];

    [[AppManager sharedManager] reverseGeocodeForCordinates:target completion:^(NSString *address) {
        if(address) {
            resultAddress = [[address componentsSeparatedByString:@","] objectAtIndex:0];
            XCTAssertEqualObjects(expectedAddress, resultAddress);
            [expectation fulfill];
        }
    }];
    [self waitForExpectationsWithTimeout:5.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
}

- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}

@end
